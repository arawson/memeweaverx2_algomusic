# duncs_node

An implementation of dunc's algomusic, by marasmusine (https://marasmusine.itch.io/duncs-algomusic) for node js. Created to enable my Discord bot to play algomusic.

Right now, this readme only describes general info about development progress, not installation or usage instructions, as the project is not even 0.0.0 yet.

# Notes on Node Implementation

## Audio Output

Discord uses Opus, so this will need to use node-opus for output to produce a stream of opus frames to push to Discord.
 * https://github.com/Rantanen/node-opus

### Required Components

  * The ability to pitch-shift sound. Algomusic uses wav files and then pitch shifts them.
    * http://blogs.zynaptiq.com/bernsee/pitch-shifting-using-the-ft/
    * https://www.gnu.org/software/gsl/ ???
      * https://directory.fsf.org/wiki/FFTw 
  * Reading wav files.
    * https://www.npmjs.com/package/wav


# Notes on Algomusic Itself

  * Constants of note frequencies are assigned at the beginning over one octave.
    * Later, the pitch function returns the correct frequency on the scale, even in other octaves.
  * Constants are also used to define which chord component is which.
    * Constant arrays are defined to house chords, minor, major, diminished, suspended, minor 7th.
    * What this `Restore chord` does I'm not sure.

# Notes on Streaming

  * [Stream Dispatcher](https://discord.js.org/#/docs/main/stable/class/StreamDispatcher) seems to be the guy.
  * [Audio Player](https://discord.js.org/#/docs/main/stable/class/AudioPlayer) controls it.

# Design 1: Native Sub-Process
```plantuml
@startuml
Bot --> SubP : Spawn
SubP --> Bot : Opus Frames
Bot --> SubP : Kill
@enduml
```
This design uses a native C++ process to send opus frames to the bot.

## Advantages
 1. Lightweight. I should be able to keep the native process's footprint very small.
 2. Familiar. Node's event-driven approach drives me up a wall sometimes.
 3. Easy Testing. I can just rebuild and then pip the output into a file. I don't need to hit discord for every test.

## Things You Will Need
 1. A Makefile and C++ program.
 2. libopus to create frames.
 3. A pipe to communicate with the bot.
 4. A library to make sound from soundfonts.
 5. A listener for the end of file signal (CTRL+D).
 6. Forward the opus frames on to discord in the bot.
