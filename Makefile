
CXXFLAGS= -std=c++17 -ggdb3 -O

CCFLAGS=-ggdb3 -O

LDFLAGS=-lopus -lsndfile

ifeq ($(OS),Windows_NT)
	CXXFLAGS+=-IC:/msys64/mingw64/include/opus
	LDFLAGS+=-lboost_filesystem-mt -lboost_system-mt
else
	LDFLAGS+=-lboost_filesystem -lboost_system
endif

BUILDDIR=build

.PHONY:
all: algomusic

algomusic: \
	$(BUILDDIR)/tsf.o \
	$(BUILDDIR)/main.o \
	$(BUILDDIR)/metronome.o \
	$(BUILDDIR)/sequence.o
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS)

$(BUILDDIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILDDIR)/%.o: %.c
	@mkdir -p $(@D)
	$(CC) $(CXXFLAGS) -c $< -o $@

.PHONY:
BUILDDIR:
	mkdir -p build

.PHONY:
clean:
	-rm -r $(BUILDDIR)
	-rm algomusic
