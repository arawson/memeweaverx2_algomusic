#pragma once

#include <cmath>
#include <vector>
#include <memory>

#include "TinySoundFont/tsf.h"

#include "sequence.hpp"

class metronome {
private:
    const float bpm;
    const int frame_millis;
    const int frame_samples;
    const int subframes;
    const int subbeats_per_beat;

    const int subframe_samples;

    const int millis_per_subbeat;
    const int subframe_advance_millis;
    int millis_to_next_sixteenth = 0;
    int subbeat = 0;

    std::vector<float> frame_buffer;
    std::vector<float*> subframe_buffers;

    std::vector<std::unique_ptr<sequence>> sequences;

public:
    metronome(float bpm, int frame_millis, int frame_samples, int sub_frames, int subbeats_per_beat);

    std::vector<float>& render(tsf* font);

    void clear_sequences();

    size_t add_sequence(std::unique_ptr<sequence>&&);

    // friend std::ostream& operator<<(std::ostream&, const metronome&);
};

// std::ostream& operator<<(std::ostream&, const metronome&);
