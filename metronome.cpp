
#include "metronome.hpp"

#include <iostream>
using std::cerr;
using std::endl;

using std::vector;

metronome::metronome(float bpm, int frame_millis, int frame_samples, int subframes,
    int subbeats_per_beat)
: bpm(bpm), frame_millis(frame_millis), frame_samples(frame_samples), subframes(subframes),
    subbeats_per_beat(subbeats_per_beat),
    subframe_samples(frame_samples/subframes),
    millis_per_subbeat((60000.f / ((float)subbeats_per_beat * (float)bpm))),
    subframe_advance_millis(frame_millis / subframes) {
    //TODO: add exception for non-integer frame_advance
    frame_buffer.reserve(frame_samples);
    for (int i = 0; i < frame_samples; i++) {
        frame_buffer.push_back(0.f);
        if (frame_samples % subframe_samples == 0) {
            subframe_buffers.push_back(&frame_buffer[i]);
        }
    }
}

std::vector<float>& metronome::render(tsf* font) {
    // gotcha! we actually advance by sub-frames
    for (int sf = 0; sf < subframes; sf++) {
        cerr << "subframe" << endl;
        //determine whether we should advance the sequences
        if (millis_to_next_sixteenth <= 0) {
            //do sequences
            cerr << "advance sequences" << endl;
            if (subbeat == 0) {
                cerr << "beat" << endl;
            }
            for (auto &s: sequences) {
                s->beat(subbeat, subbeats_per_beat, font);
            }
            subbeat++;
            subbeat %= subbeats_per_beat;

            millis_to_next_sixteenth += millis_per_subbeat;
        } else {
            // millis_to_next_sixteenth -= subframe_advance_millis;
        }
        millis_to_next_sixteenth -= subframe_advance_millis;

        {
            //render tsf to the subframe buffer
            auto buf = subframe_buffers[sf];
            tsf_render_float(font, buf, subframe_samples, 0);
        }
    }

    return frame_buffer;
}

void metronome::clear_sequences() {
    sequences.clear();
}

size_t metronome::add_sequence(std::unique_ptr<sequence>&& seq) {
    size_t newdex = sequences.size();
    sequences.push_back(std::move(seq));
    return newdex;
}

// std::ostream& operator<<(std::ostream& os, const metronome& m) {
//     return
//     os << "metronome{bpm=" << m.bpm
//     << ",ms/f=" << m.frame_millis
//     << ",ms/16th=" << m.millis_per_sixteenth
//     << ",f adv. ms=" << m.frame_advance_millis
//     << "}";
// }
