
#include <iostream>
#include <array>
#include <string>
#include <tclap/CmdLine.h>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
#include "wstring_killer.hpp"

#include <opus.h>
#include <sndfile.h>
#include "TinySoundFont/tsf.h"

#include "metronome.hpp"
#include "sequence.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::array;
using std::string;

// https://discordapp.com/developers/docs/topics/voice-connections#encrypting-and-sending-voice
//TODO: determine how to pass in frame parameters
const int FRAME_SIZE = 960; //this is 20ms at our sample rate
const int SAMPLE_RATE = 48000; // This must be 48k per discord spec. 
const int FRAME_MILLIS = ((float)FRAME_SIZE / (float)SAMPLE_RATE * 1000.f);
const int CHANNELS = 2; // This must be 2 per discord spec.
const int FRAME_SAMPLES = FRAME_SIZE * CHANNELS;
const int APPLICATION = OPUS_APPLICATION_AUDIO;
const int BITRATE = 64000;
const int MAX_FRAME_SIZE = 6*960;
const int MAX_PACKET_SIZE = 3*1276;

int main(int argc, char* argv[]) {
    fs::path soundfont_file;
    fs::path output_file;

    try {
        TCLAP::CmdLine cmd("Generates algomusic for memeweaver x2", ' ');
        TCLAP::ValueArg<string> soundfont_arg("s", "soundfont", "path to soundfont file to use", true, "", "path");
        TCLAP::ValueArg<string> output_arg("o", "output", "file or stdout to output song to", true, "", "path/string");
        // TCLAP::ValueArg<string> song_name_arg("n", "songname", "name of song to generate", true, "", "string");

        cmd.add(output_arg);
        cmd.add(soundfont_arg);

        cmd.parse(argc, argv);

        soundfont_file = soundfont_arg.getValue();
        if (!fs::is_regular_file(soundfont_file)) {
            cerr << "soundfont is not regular file" << endl;
            return -1;
        }

        output_file = output_arg.getValue();
    } catch (TCLAP::ArgException& e) {
        cerr << "argument error: " << e.error() << " for arg " << e.argId() << endl;
        return -1;
    }

    SF_INFO snd_info;
    snd_info.samplerate = SAMPLE_RATE;
    snd_info.channels = CHANNELS;
    snd_info.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;
    snd_info.sections = 1;
    snd_info.seekable = 1;
    SNDFILE *snd_file = sf_open(s_from_ws(output_file.c_str()).c_str(), SFM_WRITE, &snd_info);
    if (!snd_file) {
        cerr << "unable to open sound file: " << sf_strerror(snd_file) << endl;
        return -1;
    }

    // SndfileHandle snd_file("out.wav", SFM_WRITE,
    // SF_FORMAT_WAV | SF_FORMAT_FLOAT, CHANNELS, SAMPLE_RATE);

    cerr << "loading font..." << endl;

    tsf *font = tsf_load_filename(s_from_ws(soundfont_file.c_str()).c_str());
    if (!font) {
        cerr << "failed to load font" << endl;
        return -1;
    }
    tsf_set_output(font, TSF_STEREO_INTERLEAVED, SAMPLE_RATE, 0); //last is global gain in dB

    int err;

    // Create the encoder
    // https://chromium.googlesource.com/chromium/deps/opus/+/1.1.1/doc/trivial_example.c
    OpusEncoder *encoder = opus_encoder_create(SAMPLE_RATE, CHANNELS, APPLICATION, &err);
    if (err < 0) {
        cerr << "failed to create opus encoder: " << opus_strerror(err) << endl;
        return -1;
    }

    // Create the decoder for the temp output file
    // The file acts as our testbed.
    OpusDecoder *decoder = opus_decoder_create(SAMPLE_RATE, CHANNELS, &err);
    if (err < 0) {
        cerr << "unable to create opus decoder: " << opus_strerror(err) << endl;
        return -1;
    }

    // Set the bit-rate and other parameters if needed.
    err = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(BITRATE));
    if (err < 0) {
        cerr << "failed to set bitrate: " << opus_strerror(err) << endl;
    }

    //TODO: pass in an instrument map to load from different soundfonts
    for (int i = 0; i < tsf_get_presetcount(font); i++) {
        cerr << "preset #" << i << " is " << tsf_get_presetname(font, i) << endl;
    }

    //start notes before playing
    // tsf_note_on(font, 0, 72, 1.0f); //C4
    // tsf_note_on(font, 269, 60, 1.0f); //fatman standard drum kit / ?

    metronome m(60, FRAME_MILLIS, FRAME_SAMPLES, 4, 4);
    m.add_sequence(std::make_unique<metronome_sequence>(269, 60));

    // generate 10 seconds (500 frames) of audio
    for (size_t t = 0; t < 1000; t++) {
        cerr << ">FRAME" << endl;
        auto &buf = m.render(font);
        sf_write_float(snd_file, &buf[0], FRAME_SIZE);
        // array<float, FRAME_SIZE*CHANNELS> input;
        // array<unsigned char, MAX_PACKET_SIZE> cbits;
        // array<float, MAX_FRAME_SIZE*CHANNELS> output;

        // short out[MAX_FRAME_SIZE*CHANNELS];
        // unsigned char cbits[MAX_PACKET_SIZE];
        // unsigned char pcmbytes[MAX_FRAME_SIZE*CHANNELS*2];

        // 0 mixing for now
        // cerr << "render float from tsf" << endl;

        // tsf_render_float(font, &input[0], FRAME_SIZE*CHANNELS, 0);
        
        //now cbits should have the binary data of the frame in it
        // int nbBytes = opus_encode_float(encoder, &input[0], FRAME_SIZE, &cbits[0], MAX_PACKET_SIZE);
        // if (nbBytes < 0) {
        //     cerr << "encode failed: " << opus_strerror(nbBytes) << endl;
        //     return -1;
        // }

        // int fsize = opus_decode_float(decoder, &cbits[0], nbBytes, &output[0], MAX_FRAME_SIZE, 0);
        // if (fsize < 0) {
        //     cerr << "decode failed: " << opus_strerror(fsize) << endl;
        //     return -1;
        // }
        
        // sf_write_float(snd_file, &output[0], fsize);

        // cerr << "20ms chunk finised" << endl;
    }

    cerr << "done" << endl;

    // cerr << m << endl;

    sf_close(snd_file);

    cerr << "file closed" << endl;

    return 0;
}
