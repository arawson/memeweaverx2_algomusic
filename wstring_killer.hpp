
#pragma once

#include <string>

#include <locale>
#include <codecvt>

//sick of UCS16 on windows

inline std::string s_from_ws(const std::wstring& wstr) {
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_typeX, wchar_t> converterX;

    return converterX.to_bytes(wstr);
}

//use the overload on linux
inline std::string s_from_ws(const std::string& str) {
    return str;
}
