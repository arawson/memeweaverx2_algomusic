
#pragma once

#include "TinySoundFont/tsf.h"

class sequence {
private:

public:
    virtual ~sequence();

    virtual void beat(const int subbeat, const int subbeats_in_beat, tsf* font);
};

class metronome_sequence : public sequence {
private:
    const int preset;
    const int note;

public:
    metronome_sequence(const int preset, const int note);

    ~metronome_sequence();

    void beat(const int subbeat, const int subbeats_in_beat, tsf* font);
};
