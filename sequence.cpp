
#include "sequence.hpp"

sequence::~sequence() {}

void sequence::beat(const int subbeat, const int subbeats_in_beat, tsf* font) {}

metronome_sequence::~metronome_sequence() {}

metronome_sequence::metronome_sequence(const int preset, const int note)
: preset(preset), note(note) {}

void metronome_sequence::beat(const int subbeat, const int subbeats_in_beat, tsf* font) {
    switch (subbeat) {
        case 0:
            tsf_note_on(font, preset, note, 1.0f);
            break;
        case 1:
            // tsf_note_off(font, preset, note);
            break;
    }
}
